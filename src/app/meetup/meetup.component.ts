import { Component, OnInit, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { MeetUpEvent } from '../meetup.interface';


@Component({
  selector: 'app-meetup',
  templateUrl: './meetup.component.html',
  styleUrls: ['./meetup.component.scss']
})
export class MeetupComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    
  }

  @Input () meetup: MeetUpEvent;
  @Output()
  delete = new EventEmitter<number>();

  deleteEvent(){
    this.delete.emit(this.meetup.id)
  }

  

}

