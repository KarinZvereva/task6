import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { MeetupService } from '../meetup.service';
import { create } from 'domain';

@Component({
  selector: 'app-form-add-meetup',
  templateUrl: './form-add-meetup.component.html',
  styleUrls: ['./form-add-meetup.component.scss']
})
export class FormAddMeetupComponent implements OnInit {

  createForm = new FormGroup({
    name: new FormControl('', Validators.required),
    description: new FormControl('', Validators.minLength(10)),
    address: new FormControl('', Validators.required),
    date: new FormControl('', Validators.required),
    published: new FormControl(false),
  });


  constructor(private location: Location, private service: MeetupService) { }

  ngOnInit() {

    this.createForm.get('name').valueChanges.subscribe(value => {
      console.log('name value', value);
  });

  this.createForm.get('published').valueChanges.subscribe(value => {
    if (value) {
      this.createForm.get('name').enable();
    } else {
      this.createForm.get('name').disable();
    }});
  }

  onSubmit(){
    this.service.addMeetup(this.createForm.value);
    this.location.back();
  }

  get desc(){
    return this.createForm.get('description');
  }
}
