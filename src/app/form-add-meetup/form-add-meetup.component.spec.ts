import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAddMeetupComponent } from './form-add-meetup.component';

describe('FormAddMeetupComponent', () => {
  let component: FormAddMeetupComponent;
  let fixture: ComponentFixture<FormAddMeetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAddMeetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAddMeetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
