import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MeetupComponent } from './meetup/meetup.component';
import { AboutComponent } from './about/about.component';
import { ListMeetupComponent } from './list-meetup/list-meetup.component';
import { FormAddMeetupComponent } from './form-add-meetup/form-add-meetup.component';
import { MeetupPageComponent } from './meetup-page/meetup-page.component';

const routes: Routes = [{
  path: '',
  redirectTo: 'about',
  pathMatch: 'full'
},{
  component: ListMeetupComponent,
  path: 'meetup',
  children: [
    {
      component: FormAddMeetupComponent,
      path: 'add'
    }
  ]
},
{
  component: AboutComponent,
  path: 'about'
},
{
  component: MeetupPageComponent,
  path: 'meetup/:id'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
