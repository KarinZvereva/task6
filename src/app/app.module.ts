import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatToolbarModule, MatIconModule,MatChipsModule, MatButtonModule, MatCardModule, MatGridListModule, MatMenuModule, MatInputModule, MatCheckboxModule } from '@angular/material';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MeetupComponent } from './meetup/meetup.component';
import { AboutComponent } from './about/about.component';
import { ListMeetupComponent } from './list-meetup/list-meetup.component';
import { FormAddMeetupComponent } from './form-add-meetup/form-add-meetup.component';
import { MeetupPageComponent } from './meetup-page/meetup-page.component';
import { ReactiveFormsModule } from "@angular/forms";
@NgModule({
  declarations: [
    AppComponent,
    MeetupComponent,
    AboutComponent,
    ListMeetupComponent,
    FormAddMeetupComponent,
    MeetupPageComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    MatChipsModule,
    MatMenuModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCheckboxModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
