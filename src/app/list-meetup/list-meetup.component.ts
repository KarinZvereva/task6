import { Component, OnInit } from '@angular/core';
import { MeetUpEvent } from '../meetup.interface';
import { meetupList } from '../meetup';
import { MeetupService } from '../meetup.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-list-meetup',
  templateUrl: './list-meetup.component.html',
  styleUrls: ['./list-meetup.component.scss']
})
export class ListMeetupComponent implements OnInit {
  meetupList: MeetUpEvent[];

  constructor(private service: MeetupService) {}


  ngOnInit(){
    this.update();
  }

  deleteEvent(id: number){
    this.service.deleteMeetup(id);
    this.update();
  }

  update(){
    this.meetupList = this.service.getMeetuList();
  }
}
