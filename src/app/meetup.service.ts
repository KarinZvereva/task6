import { Injectable } from '@angular/core';
import { MeetUpEvent } from './meetup.interface';
import { meetupList } from './meetup';

@Injectable({
  providedIn: 'root'
})
export class MeetupService {

  meetupList: MeetUpEvent[] = meetupList;

  constructor() { }

  getMeetuList(): MeetUpEvent[]{
    return this.meetupList.slice();
  }

  deleteMeetup(id: number){
    this.meetupList = this.meetupList.filter(e => e.id!==id);
  }

  addMeetup(meetup : MeetUpEvent){
    meetup.id = ++this.getMeetuList().length;
    this.meetupList.push(meetup);
  }
}
